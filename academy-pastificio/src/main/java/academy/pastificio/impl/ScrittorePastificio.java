package academy.pastificio.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import academy.pastificio.datarecord.DataField;
import academy.pastificio.datarecord.DataRecord;
import academy.pastificio.datarecord.DataRecordException;

public class ScrittorePastificio {

	private String dati;
	private DataRecord[] dataRecords;
	private int numeroRecordRitornati;

	public ScrittorePastificio(String dati) throws DataRecordException {
		this.dati = dati;
		this.dataRecords = this.read();
	}

	private DataRecord[] read() throws DataRecordException {

		/*
		 * variabili usate per creare elenchi di DataRecord
		 */
		List<DataRecord> dataRecordList = new ArrayList<>();
		DataRecord[] dataRecordArray = null; // variabile restituita dal metodo

		/*
		 * variabili usate dall'intestazione
		 */
		String[] arrayIntestazione;
		String[] arrayDati;
		String path = "C:\\Users\\alessandro.izzo\\Documents\\workspace-spring-tool-suite-4-4.14.0.RELEASE\\academy-pastificio\\src\\main\\resources\\fileScritto.txt";
		
		try (
				/*
				 * apro un canale di comunicazione verso la fonte dati
				 */
				
				FileWriter fileWriter = new FileWriter(path);
				/*
				 * ci applico un decoratore: mi aiuta a semplificarmi il lavoro di recupero
				 * righe
				 */
				BufferedWriter bufferedReader = new BufferedWriter(fileWriter);) {
			
			String[] datiSplittati = dati.split("\n");
			String intestazione = datiSplittati[0];
			
			bufferedReader.write(intestazione);
			
			
			
			
			
			
			
			
			List<String> righeDatiList = new ArrayList<String>();
			String rigaDati = null;
			while ((rigaDati = bufferedReader.readLine()) != null) {
				righeDatiList.add(rigaDati);
			}
			
			String[] righeDati = righeDatiList.toArray(new String[] {});

			/*
			 * intestazione
			 */
			arrayIntestazione = righeDati[0].split(","); // [Nome|Cognome|Eta]

			/*
			 * dati
			 */
			for (int i = 1; i < righeDati.length; i++) {

				String rigaDati = righeDati[i]; // 1) [Flavio,Giannini,47] 2) ...

				List<DataField> dataFields = new ArrayList<>(); // size: elementi che ci sono dentro --- capacity:
																// capacit

				arrayDati = rigaDati.trim().split(",");// Patrizio,Cervigni,34 --> [Patrizio,Cervigni,34]

				/*
				 * eseguo esattamente 4 cicli per ogni arrayDati perch l'array composto da 3
				 * elementi
				 */
				for (int j = 0; j < arrayDati.length; j++) {

					/*
					 * creo field
					 */
					DataField dataField = new DataField(arrayIntestazione[j], arrayDati[j]);

					/*
					 * aggiungo il field alla lista di DataField
					 */
					dataFields.add(dataField);

				}

				/*
				 * creo un data Record che contiene 3 data fields
				 * 
				 * come parametro trasformo una lista in array
				 */
				DataRecord dataRecord = new DataRecord(dataFields.toArray(new DataField[arrayDati.length]));

				/*
				 * aggiungo il data record alla lista
				 */
				dataRecordList.add(dataRecord);

			}

			dataRecordArray = dataRecordList.toArray(new DataRecord[dataRecordList.size()]);

		} catch (IOException e) {
			throw DataRecordException.erroreLetturaDati(e);
		}

		return dataRecordArray;
	

		
	}
}
