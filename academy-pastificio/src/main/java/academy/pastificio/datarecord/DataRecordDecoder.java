package academy.pastificio.datarecord;

/**
 * Componente di decodifica di un flusso dati in record
 * 
 * @author Flavio Giannini
 *
 */
public interface DataRecordDecoder {

	/**
	 * Ritorna il record successivo
	 * 
	 * @return
	 * @throws DataRecordException Viene sollevata qualora sia stato gi� letto
	 *                             l'ultimo record
	 */
	DataRecord nextRecord() throws DataRecordException;

	/**
	 * Ritorna vero se esiste il record successivo
	 * 
	 * @return
	 * @throws DataRecordException
	 */
	boolean hasNext() throws DataRecordException;

}
