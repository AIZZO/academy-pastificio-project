package academy.pastificio.datarecord;



public class DataRecordException extends Exception{

	private DataRecordException(String message) {
		super(message);
	}
	
	public static DataRecordException recordInesistente() {
		return new DataRecordException("record inesistente");
	}
	
	public static DataRecordException fieldInesistente() {
		return new DataRecordException("field inesistente");
	}

	public static DataRecordException erroreLetturaDati(Throwable cause) {
		return new DataRecordException("erroreLetturaDati: " + cause.getMessage());
	}


	public static DataRecordException  decoderNonInizializzato() {
		return new DataRecordException("decoder non inizializzato");
	}
	
	

}
