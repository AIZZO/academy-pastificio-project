package academy.pastificio.run;

import academy.pastificio.datarecord.DataRecordException;
import academy.pastificio.impl.ScrittorePastificio;

public class Main {

	public static void main(String[] args) throws DataRecordException {

		String dati = "CODICE,NOME,DESCRIZIONE,PREZZO,CODICE_MARCA\nB001\nSpaghetti Barilla 500g\nConfezione spaghetti di grano leggero pasta Barilla di 250g\n1,00\nBARILLA\nB002\nRigatoni Barilla 250g\nConfezione rigatoni di grano leggero pasta Barilla di 250g\n1,00\nBARILLA";

		ScrittorePastificio scrittore = new ScrittorePastificio(dati);

	}

}
